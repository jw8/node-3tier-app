# Sample 3tier app
This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
Cloudfront(CDN) <=> web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.

Web Endpoint: https://www.toptal.xomar.com/
API Endpoint: https://api.toptal.xomar.com/

# Summary
The 3tier app is deployed onto AWS using terraform, it leverages ECS, RDS, ALB and cloud front. The application code is stored in gitlab and deployments are kicked off by successful builds. Gitlab automatically builds and deploys the application to ECS. There are two ECS instances configured and each application is configured for high availability. Deploys will not impact availability. Application logs and saved to cloudwatch so they can be reviewed in real time, as well as application metrics. Cloudfront forwards reqeusts to an application load balancer. Pages are cached up to 1 day. This behavior can be changed if the application correctly forwards `Cache-Control` headers. Cloudfront & ALB logs land in a s3 bucket `toptal-logging`. 

## Terraform
This project is built with the latest version of terraform, 12.6 and requires a working AWS credentials. Every terraform module is broken out into seperate modules. All modules are built with low cost in mind and focused on reusability. A Production deployment should include multi az RDS & likely more ECS instances. These values can be changed in their respective modules.

### Deploying infrastructure via Terraform
There are numerous terraform files which need to be init'd and applied. The initial terraform state resoruces must be created first and some files must be moved around to prevent a chicken or egg situtation.
#### terraform-assets
in terraform/aws/dev/us-east-1/terraform-assets
1. `mv init.tf init.tf2`
2. `terraform init && terraform apply`
3. `mv init.tf2 init.tf`
4. `terraform import aws_s3_bucket.tf-assets toptal-devops && terraform import aws_dynamodb_table.terraform_state_lock terraform-lock`
5. terraform apply

#### terraform-infra
The following order, you will need to run `terraform init` & `terraform apply` and say yes to any changes. in 
1. terraform/aws/dev/global
2. terraform/aws/dev/us-east-1/networking
3. terraform/aws/dev/us-east-1/acm
4. terraform/aws/dev/us-east-1/alb
5. terraform/aws/dev/us-east-1/ecr
6. terraform/aws/dev/us-east-1/sns-alarm
7. terraform/aws/dev/us-east-1/database
8. terraform/aws/dev/us-east-1/ecs
9. terraform/aws/dev/us-east-1/ecs-apps/toptal-api
10. terraform/aws/dev/us-east-1/ecs-apps/toptal-web

The acm module might timeout due to route53 & certificate valiation. Go to the ACM page to verify that the cert was validated.
The toptal-web module will take a while to deploy due to cloudformation taking up to 15 minutes to deploy.

If scaling out for a production deployment, enable multi AZ in the terraform/aws/dev/us-east-1/databases/vars.tf & run `terraform apply`

## Credentials
All credentials are stored in AWS SSM & encrypted with a AWS managed key in KMS.

## Configuring CI/CD
The terraform global module located in `terraform/dev/us-east-1/global` Configures the IAM user that should be used when setting up CI/CD. Terraform will create the user and the policies but will not generate the AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY for security concerns. The developer should generate them via the console and use those credentials.


## Backups
The database instance is backed up every day at "03:00-06:00" UTC. The databases can be restored via the RDS restore snapshot.
https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_RestoreFromSnapshot.html
The application code is stored in gitlab and on the developer laptop and is therefore distributed to numerous places, no other backups are required as the code & database holds all of the state.

## Alerting
A cloudwatch alarm is created when the database uses up 80% of the available disk space. You will need to subscribe yourself to the SNS topic.