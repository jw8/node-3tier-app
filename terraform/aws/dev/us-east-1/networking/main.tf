module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "toptal"
  cidr = "10.80.0.0/16"

  azs              = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets  = ["10.80.1.0/24", "10.80.2.0/24", "10.80.3.0/24"]
  public_subnets   = ["10.80.101.0/24", "10.80.102.0/24", "10.80.103.0/24"]
  database_subnets = ["10.80.21.0/24", "10.80.22.0/24"]


  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}