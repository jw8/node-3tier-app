resource "aws_sns_topic" "sns-alarm" {
  name = "sns-alarm"

  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}