data "terraform_remote_state" "alb" {
  backend = "s3"
  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/alb/tf.state"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "global" {
  backend = "s3"
  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/global/tf.state"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "ecs" {
  backend = "s3"
  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/ecs/tf.state"
    region = "us-east-1"
  }
}


data "terraform_remote_state" "database" {
  backend = "s3"
  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/database/tf.state"
    region = "us-east-1"
  }
}

data "aws_ssm_parameter" "database_user" {
  name = data.terraform_remote_state.database.outputs.rds-user-ssm-path
}

data "aws_ssm_parameter" "password" {
  name = data.terraform_remote_state.database.outputs.rds-password-ssm-path
}

data "terraform_remote_state" "networking" {
  backend = "s3"
  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/networking/tf.state"
    region = "us-east-1"
  }
}