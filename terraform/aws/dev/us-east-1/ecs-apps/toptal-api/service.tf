
locals {
  //username:password@localhost/database
  db_string = "${format("%s:%s@%s/%s",
    data.aws_ssm_parameter.database_user.value,
    data.aws_ssm_parameter.password.value,
    data.terraform_remote_state.database.outputs.rds-fqdn,
    data.terraform_remote_state.database.outputs.rds-database)}"
}



resource "aws_cloudwatch_log_group" "toptal-api" {
  name              = "toptal-api"
  retention_in_days = 1
}

resource "aws_ecs_task_definition" "toptal-api" {
  family = "toptal-api"

  container_definitions = <<EOF
[
  {
    "name": "toptal-api",
    "image": "796056436839.dkr.ecr.us-east-1.amazonaws.com/toptal-api:latest",
    "cpu": 0,
    "memory": 128,
    "environment": [
        {
            "name": "PORT",
            "value": "8080"
        },
        {
            "name": "DB",
            "value": "postgres://${local.db_string}"
        }
    ],
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "us-east-1",
        "awslogs-group": "toptal-api"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "toptal-api" {
  name            = "toptal-api"
  cluster         = data.terraform_remote_state.ecs.outputs.cluster_name
  task_definition = aws_ecs_task_definition.toptal-api.arn

  desired_count = 2

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 50
  load_balancer {
    target_group_arn = "${aws_lb_target_group.toptal-api.arn}"
    container_name   = "toptal-api"
    container_port   = 8080
  }
}