resource "aws_route53_record" "this" {
  zone_id = data.terraform_remote_state.global.outputs.zone_id
  name    = "api"
  type    = "A"

  alias {
    name                   = data.terraform_remote_state.alb.outputs.alb_dns_name
    zone_id                = data.terraform_remote_state.alb.outputs.alb_zone_id
    evaluate_target_health = true
  }
}

resource "aws_lb_listener_rule" "this" {
  listener_arn = data.terraform_remote_state.alb.outputs.ext_443_listener_arn

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.toptal-api.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${aws_route53_record.this.fqdn}"]
  }
}

resource "aws_lb_target_group" "toptal-api" {
  name     = "toptal-api"
  port     = 8080
  protocol = "HTTP"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id
  health_check {
    enabled = true
    path = "/api/status"

  }
}
