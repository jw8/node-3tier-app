resource "aws_route53_record" "origin" {
  zone_id = data.terraform_remote_state.global.outputs.zone_id
  name    = "origin"
  type    = "A"

  alias {
    name                   = data.terraform_remote_state.alb.outputs.alb_dns_name
    zone_id                = data.terraform_remote_state.alb.outputs.alb_zone_id
    evaluate_target_health = true
  }
}

resource "aws_lb_listener_rule" "origin" {
  listener_arn = data.terraform_remote_state.alb.outputs.ext_443_listener_arn

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.toptal-web.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${aws_route53_record.origin.fqdn}"]
  }
}

resource "aws_lb_listener_rule" "www" {
  listener_arn = data.terraform_remote_state.alb.outputs.ext_443_listener_arn

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.toptal-web.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${aws_route53_record.www.fqdn}"]
  }
}

resource "aws_lb_target_group" "toptal-web" {
  name     = "toptal-web"
  port     = 8081
  protocol = "HTTP"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id
}
