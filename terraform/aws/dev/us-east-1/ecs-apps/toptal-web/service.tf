resource "aws_cloudwatch_log_group" "toptal-web" {
  name              = "toptal-web"
  retention_in_days = 1
}

resource "aws_ecs_task_definition" "toptal-web" {
  family = "toptal-web"

  container_definitions = <<EOF
[
  {
    "name": "toptal-web",
    "image": "796056436839.dkr.ecr.us-east-1.amazonaws.com/toptal-web:latest",
    "cpu": 0,
    "memory": 128,
    "environment": [
        {
            "name": "PORT",
            "value": "8081"
        },
        {
            "name": "API_HOST",
            "value": "https://api.toptal.xomar.com"
        }
    ],
    "portMappings": [
      {
        "containerPort": 8081,
        "hostPort": 8081
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "us-east-1",
        "awslogs-group": "toptal-web"
      }
    }
  }
]
EOF
}

resource "aws_ecs_service" "toptal-web" {
  name            = "toptal-web"
  cluster         = data.terraform_remote_state.ecs.outputs.cluster_name
  task_definition = aws_ecs_task_definition.toptal-web.arn

  desired_count = 2

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 50
  load_balancer {
    target_group_arn = "${aws_lb_target_group.toptal-web.arn}"
    container_name   = "toptal-web"
    container_port   = 8081
  }
}
