resource "aws_route53_record" "www" {
  zone_id = data.terraform_remote_state.global.outputs.zone_id
  name    = "www"
  type    = "A"

  alias {
    name = "${aws_cloudfront_distribution.toptal-web.domain_name}"
    zone_id = "${aws_cloudfront_distribution.toptal-web.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_cloudfront_distribution" "toptal-web" {
  aliases = ["www.toptal.xomar.com"]
  origin {
    domain_name = "origin.toptal.xomar.com"
    origin_id = "website_access_id"
  
    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols = ["TLSv1.2"]
    }
  }
  enabled = true

  logging_config {
    include_cookies = false
    bucket          = "toptal-logging.s3.amazonaws.com"
  }

  default_cache_behavior {
    allowed_methods = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods = ["HEAD", "GET"]
    compress = true
    target_origin_id = "website_access_id"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl     = 0
    default_ttl = 3600
    max_ttl     = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Terraform = "true"
    project   = "toptal"
  }

  viewer_certificate {
    acm_certificate_arn = data.terraform_remote_state.acm.outputs.acm_arn
    ssl_support_method = "sni-only"
  }


}
