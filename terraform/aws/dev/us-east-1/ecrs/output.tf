output "toptal-web_url" {
  value = "${aws_ecr_repository.toptal-web.repository_url}"
}

output "toptal-api_url" {
  value = "${aws_ecr_repository.toptal-api.repository_url}"
}
