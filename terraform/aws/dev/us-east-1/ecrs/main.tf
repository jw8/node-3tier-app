resource "aws_ecr_repository" "toptal-web" {
  name = "toptal-web"
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}

resource "aws_ecr_repository" "toptal-api" {
  name = "toptal-api"
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}