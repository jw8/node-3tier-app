resource "aws_security_group" "rds" {
  name        = "rds-default"
  description = "rds-default"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  ingress {
    from_port                = 5432
    to_port                  = 5432
    protocol                 = "tcp"
    security_groups = ["${aws_security_group.rds-access.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}

resource "aws_security_group" "rds-access" {
  name        = "rds-access"
  description = "Allows access to databases in the RDS security group"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}