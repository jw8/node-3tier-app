data "aws_caller_identity" "current" {}

data "terraform_remote_state" "networking" {
  backend = "s3"

  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/networking/tf.state"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "sns-alarm" {
  backend = "s3"

  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/sns-alarm/tf.state"
    region = "us-east-1"
  }
}
