resource "random_string" "psql_pass" {
  length  = 12
  special = false
}

resource "aws_ssm_parameter" "db-passwd" {
  name      = "/db/dataSource/password"
  type      = "SecureString"
  value     = "${random_string.psql_pass.result}"
  overwrite = "false"
}

resource "aws_ssm_parameter" "db-user" {
  name      = "/db/dataSource/username"
  type      = "String"
  value     = "postgres"
  overwrite = "false"
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "toptal"

  engine            = "postgres"
  engine_version    = "11.4"
  instance_class    = "${var.rds_class}"
  allocated_storage = "${var.rds_storage}"


  ## Use default rds kms key. Some instnaces do not support encryption. 
  storage_encrypted = "${var.rds_encryption}"
  # If kms key not specified, will default to the rds KMS
  #kms_key_id        = "arm:aws:kms:us-east-2:${data.aws_caller_identity.current.account_id}:alias/aws/rds"
  name = "toptal"

  username = "${aws_ssm_parameter.db-user.value}"

  password = "${aws_ssm_parameter.db-passwd.value}"
  port     = "5432"

  vpc_security_group_ids = ["${aws_security_group.rds.id}"]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Terraform = "true"
    project   = "toptal"
  }

  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

  # Should enable Multi az for production deployments.
  multi_az = false
  # DB subnet group
  subnet_ids = data.terraform_remote_state.networking.outputs.database_subnets

  # DB parameter group
  family = "postgres11"

  # DB option group
  major_engine_version = "11.4"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "toptal"

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "log_min_duration_statement"
      value = "1000"
    },
    {
      name  = "log_statement"
      value = "ddl"
    },
  ]

  options = []
}

resource "aws_cloudwatch_metric_alarm" "db_storage" {
  alarm_name          = "rds-storage"
  evaluation_periods  = "2"
  comparison_operator = "LessThanOrEqualToThreshold"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Minimum"
  threshold           = "${var.rds_storage * 0.2}"
  alarm_description   = "Alert generated if the DB has less than a 20% of storage left"
  alarm_actions       = [data.terraform_remote_state.sns-alarm.outputs.monitoring]
  ok_actions          = [data.terraform_remote_state.sns-alarm.outputs.monitoring]

  dimensions = {
    DBInstanceIdentifier = "${module.db.this_db_instance_id}"
  }
}