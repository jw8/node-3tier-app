variable "rds_storage" {
  default = 20
}

# Using a t2 large or greater allows for at rest database encryption
variable "rds_class" {
  default = "db.t2.medium"
}
# AWS RDS encryption information
# https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.Encryption.html#Overview.Encryption.Enabling
variable "rds_encryption" {
  default = "false"
}
