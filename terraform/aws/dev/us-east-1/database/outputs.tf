output "rds-sg" {
  value = "${aws_security_group.rds.id}"
}

output "rds-fqdn" {
  value = "${module.db.this_db_instance_endpoint}"
}

output "rds-database" {
  value = "${module.db.this_db_instance_name}"
}

output "rds-user-ssm-path" {
  value = "${aws_ssm_parameter.db-user.name}"
}

output "rds-password-ssm-path" {
  value = "${aws_ssm_parameter.db-passwd.name}"
}

output "rds-access" {
  value = "${aws_security_group.rds-access.id}"
}