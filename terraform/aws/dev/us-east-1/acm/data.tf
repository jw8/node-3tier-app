data "terraform_remote_state" "global" {
  backend = "s3"

  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/global/tf.state"
    region = "us-east-1"
  }
}