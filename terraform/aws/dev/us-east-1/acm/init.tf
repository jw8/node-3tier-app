terraform {
  backend "s3" {
    bucket         = "toptal-devops"
    key            = "terraform/dev/us-east-1/acm/tf.state"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.23.0"
}
