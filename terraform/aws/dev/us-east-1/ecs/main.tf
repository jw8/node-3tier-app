resource "aws_key_pair" "us-east-1-ecs-key" {
  key_name   = "us-east-1-ecs-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxAPTA3zIWoCdU9nj3WNxH3gDIuRumjNx9eMe0PNMuV1ZuJh887weKEftuhYUW6wt8x0BJ8GYV3sDQ5W9K4DmeEFUi5eOyjDMjyIwffNgNxkMmPeR00Rqr3jP6zxH9BTPy4UOPpP/sgsAr6p1VDxQ1jdywLeJB6T7kfvtItlVtKRvfz4GW0NbExfvmpx3Vxv25zHh+AO1SnWIxhvJJbBfASYTfjsobE6IK30B4g+XR6THZjNeEJar+iOyZZppMcgRBaS3yWLNPZ6tUhZdkGDxoluhOGTJc/uVIgvyvl83K5Bfa/MF6BjhzT6Ln9feD/Le9WUp/9OmJTrZnV7VBFyF7 jw@DESKTOP-Q554MN1"
}

resource "aws_ecs_cluster" "cluster" {
  name = "toptal"
}

resource "aws_launch_configuration" "ecs" {
  name_prefix                 = "toptal-ecs"
  image_id                    = "${data.aws_ami.ecs.id}"
  instance_type               = "${var.ecs_type}"
  key_name                    = "${aws_key_pair.us-east-1-ecs-key.key_name}"
  associate_public_ip_address = "false"

  security_groups = [
    "${aws_security_group.http-access.id}",
    data.terraform_remote_state.database.outputs.rds-access,
  ]

  iam_instance_profile = "${aws_iam_instance_profile.ecs.arn}"
  user_data            = "${data.template_file.ecs.rendered}"

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "30"
    delete_on_termination = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs" {
  name                 = "toptal-ecs"
  max_size             = "2"
  min_size             = "1"
  desired_capacity     = "2"
  launch_configuration = "${aws_launch_configuration.ecs.name}"
  vpc_zone_identifier  = data.terraform_remote_state.networking.outputs.private_subnets

  tags = [
    {
      key                 = "Name"
      value               = "toptal-ecs"
      propagate_at_launch = true
    },
    {
      key                 = "Terraform"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "project"
      value               = "toptal"
      propagate_at_launch = true
    },
  ]
}

data "template_file" "ecs" {
  template = "${file("${path.module}/templates/ecs.sh")}"

  vars = {
    ECS_CLUSTER = "${aws_ecs_cluster.cluster.name}"
  }
}

resource "aws_security_group" "http-access" {
  name        = "http-access"
  description = "http-access"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}