data "aws_caller_identity" "current" {}

data "terraform_remote_state" "networking" {
  backend = "s3"

  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/networking/tf.state"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "database" {
  backend = "s3"

  config = {
    bucket = "toptal-devops"
    key    = "terraform/dev/us-east-1/database/tf.state"
    region = "us-east-1"
  }
}

data "aws_ami" "ecs" {
  most_recent = true
  name_regex  = "^amzn-ami-.*amazon-ecs-optimized$"
  owners      = ["591542846629"]
}

data "aws_elb_service_account" "us-east-1" {}