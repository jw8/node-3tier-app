output "public_http_sg_id" {
  value = aws_security_group.public-http.id
}

output "alb_dns_name" {
  value = aws_lb.this.dns_name
}

output "alb_zone_id" {
  value = aws_lb.this.zone_id
}

output "ext_443_listener_arn" {
  value = aws_lb_listener.this-443.arn
}

