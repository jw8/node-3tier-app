resource "aws_s3_bucket" "logging" {
  bucket = "toptal-logging"

  tags = {
    Terraform = "true"
    project   = "toptal"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_policy" "logging-policy" {
  bucket = "${aws_s3_bucket.logging.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
      {
        "Action": [
          "s3:PutObject"
        ],
        "Effect": "Allow",
        "Resource": "${aws_s3_bucket.logging.arn}/*",
        "Principal": {
          "AWS": [
            "${data.aws_elb_service_account.us-east-1.arn}"
           ]
        }
      }
  ]
}
POLICY
}