resource "aws_security_group" "public-http" {
  name        = "public-http"
  description = "Allows communication from the world to 80 & 443"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id
}

resource "aws_security_group_rule" "external-out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.public-http.id
}

resource "aws_security_group_rule" "http-in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.public-http.id
}

resource "aws_security_group_rule" "https-in" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.public-http.id
}

