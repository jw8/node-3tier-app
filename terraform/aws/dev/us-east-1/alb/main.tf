resource "aws_lb" "this" {
  name               = "toptal-http"
  load_balancer_type = "application"
  subnets            = data.terraform_remote_state.networking.outputs.public_subnets

  security_groups = [
    "${aws_security_group.public-http.id}",
  ]

  tags = {
    Terraform = "true"
    project   = "toptal"
  }

  access_logs {
    bucket = "${aws_s3_bucket.logging.id}"
    enabled = true
  }
  depends_on = ["aws_s3_bucket_policy.logging-policy"]

}

resource "aws_lb_listener" "this-80" {
  load_balancer_arn = "${aws_lb.this.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "this-443" {
  load_balancer_arn = "${aws_lb.this.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn   = data.terraform_remote_state.acm.outputs.acm_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      status_code  = 404
    }
  }
}
