resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "terraform-lock"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "LockID"
	attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    project = "toptal"
  }
}

resource "aws_s3_bucket" "tf-assets" {
  bucket = "toptal-devops"
  tags = {
    project = "toptal"
  }
server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
}