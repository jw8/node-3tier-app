resource "aws_route53_zone" "toptal" {
  name = "toptal.xomar.com"
  tags = {
    Terraform = "true"
    project   = "toptal"
  }
}