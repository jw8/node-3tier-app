resource "aws_iam_user" "gitlab-ci" {
  name = "gitlab-ci"
  path = "/gitlab/"
}

resource "aws_iam_user_policy" "gitlab" {
  name = "gitlab"
  user = "${aws_iam_user.gitlab-ci.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_user_policy_attachment" "ecs-admin" {
  user       = "${aws_iam_user.gitlab-ci.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerServiceFullAccess"
}

resource "aws_iam_user_policy_attachment" "ecr-admin" {
  user       = "${aws_iam_user.gitlab-ci.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}