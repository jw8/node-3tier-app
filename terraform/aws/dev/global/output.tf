output "zone_id" {
  value = "${aws_route53_zone.toptal.zone_id}"
}
output "nameservers" {
  value = "${aws_route53_zone.toptal.name_servers}"
}
